=head1 NAME

singledelta - heuristic minimizer of interesting files

=head1 SYNOPSIS

B<singledelta> -test=I<test-script> [options] [I<initial-file>]

=head1 DESCRIPTION

Delta assists you in minimizing "interesting" files subject to a test of their
interestingness. A common such situation is when attempting to isolate a small
failure-inducing substring of a large input that causes your program to exhibit
a bug.

You supply B<singledelta> with

=over 2

=item *

a test shell script which decides if its input file is "interesting"
(I<test-script>) and

=item *

an initial interesting input file (I<initial-file> or standard input).

=back

Delta uses heuristics to find a sub-file of your input file that is still
"interesting" according to your test.

Delta has a notion of the granularity of the file: the smallest atomic elements
of which the file is seen as a sequence. The default is the line granularity:
in this mode, delta will attempt to delete entire lines, but will never try
deleting a smaller element than that. You can filter a program through
L<topformflat(1)> to produce a file where the line-granularity only goes to a
specified nesting depth (if your file is in a nested language).
L<multidelta(1)> does this for you.

=head1 OPTIONS

=over 4

=item B<-test>=I<test-script>

Use I<test-script> as the test program. This option is mandatory.

The test program accepts a single argument, the name of the candidate file to
test. It is run within a directory containing only that file, and it can make
temporary files/directories in that directory. It should return zero for a
candidate that exhibits the desired property, and nonzero for one that does
not.

=item B<-suffix>=I<suffix>

Set filename suffix to I<suffix>. The default is C<.c>.

=item B<-dump_input>

Dump the contents of the initial file after reading it.

=item B<-cp_minimal>=I<file>

Copy the minimal successful test to I<file>.

=item B<-granularity>=B<line>

Use lines as the granularity. This is the default.

=item B<-granularity>=B<top_form>

Use C top-level forms as the granularity.
This currently only works with CIL output.

=item B<-log>=I<file>

Log main events to I<file>.

=item B<-quiet>

Do not produce any output.

=item B<-verbose>

Produce more verbose output.

=item B<-in_place>

Overwrite I<initial-file> with inputs.

=item B<-help>

Print out usage information.

=back

=head1 NOTES

The name B<singledelta> is Debian-specific.
The upstream name of this program is simply B<delta>.

=head1 SEE ALSO

L<multidelta(1)>, L<topformflat(1)>

L<Delta Debugging project|http://www.st.cs.uni-sb.de/dd/>

=cut
